# coding=utf-8
import requests
import argparse
import random
import time
import json
import re

from pymongo import MongoClient
from bs4 import BeautifulSoup
from user_agent import generate_user_agent
from multiprocessing.pool import ThreadPool as Pool


class YellowPageScraper:
    # 0 - search term
    # 1 - city
    # 2 - state
    search_url = "http://www.yellowpages.com/search?search_terms={0}&geo_location_terms={1}%2C+{2}"
    # client = MongoClient()
    # db = client.yellowpage

    def __init__(self, term):
        self.url = None
        self.html = None
        self.count = None

        # self.cursor = self.db.city_state_business.find({"CityID": 300})
        self.term = term

    def search(self):

        # for i, term in enumerate(self.cursor):
        self.url = self.search_url.format(self.term['Kind'], self.term['Name'], self.term['State'])
        self.html = _get_html_page(self.url)
        self.parse_html()
        print("Count:", self.count)

    def parse_html(self):
        soup = BeautifulSoup(self.html, "lxml")
        try:
            results = soup.find('div', class_="pagination").find('p').text
        except AttributeError:
            results = None
        self.count = results


def _get_html_page(get_url):
    """Requests HTML from URL"""
    headers = {
        'Accept-Language': "en-US;q=0.8,en;q=0.6",
        'Origin': 'http://www.yellowpages.com/',
        'User-Agent': generate_user_agent()
    }
    try:
        query = requests.get(get_url, headers=headers)
    except:
        try:
            time.sleep(5)
            query = requests.get(get_url, headers=headers, verify=False)
        except:
            return None
    html = query.text
    del query
    return html


def get_data_from_db():
    client = MongoClient()
    db = client.yellowpage

    # cursor = db.city_state_business.find({"CityID": 300})
    cursor = db.city_state_business.find()

    return cursor


def searcher(cursor):
    spider = YellowPageScraper(cursor)
    spider.search()


def main():
    start_time = time.time()

    # spider = YellowPageScraper()
    # spider.search()
    data = get_data_from_db()

    pool = Pool(8)

    results = pool.map(searcher, data[:1000])

    data.close()

    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == '__main__':
    main()

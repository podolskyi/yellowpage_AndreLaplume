# -*- coding: utf-8 -*-
import csv


kindfile = open('kind.csv', encoding="utf-8")
kindreader = csv.reader(kindfile, delimiter=',', quotechar='|')
kinds = []
for row in kindreader:
    kinds.append(row)


cityfile = open('city.csv', encoding="utf-8")
cityreader = csv.reader(cityfile, delimiter=',', quotechar='|')
cities = []
for row in cityreader:
    cities.append(row)

res = [['city_id', 'city', 'province', 'kind_id', 'kind']]
for kind in kinds:
    for city in cities:
        res.append(city + kind)

print('count row', len(res))

result_file = open('canada_scraper.csv', 'w', encoding="utf-8")
spamwriter = csv.writer(result_file, delimiter=',', lineterminator='\n', quotechar='|', quoting=csv.QUOTE_MINIMAL)
for row in res:
    spamwriter.writerow(row)

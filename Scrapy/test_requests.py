# -*- coding: utf-8 -*-
import requests

from user_agent import generate_user_agent
from bs4 import BeautifulSoup

url = "http://www.yelp.com/search?find_loc=San+Francisco,+CA"

headers = {
        'User-Agent': generate_user_agent()
    }

r = requests.get(url, headers=headers)

soup = BeautifulSoup(r.text, "lxml")

rows = soup.find_all('li', class_='regular-search-result')

for row in rows:
    name = row.find('a', class_="biz-name")
    if name:
        print(name.text.strip())
    address = row.find('address')
    if address:
        print(address.text.strip())
    biz_phone = row.find('span', class_="biz-phone")
    if biz_phone:
        print(biz_phone.text.strip())
    print('---------------------------------------')

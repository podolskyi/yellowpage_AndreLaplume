# -*- coding: utf-8 -*-

# Scrapy settings for yellowpage project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'yellowpage'

SPIDER_MODULES = ['yellowpage.spiders']
NEWSPIDER_MODULE = 'yellowpage.spiders'

# HTTPERROR_ALLOWED_CODES = [404, 503, 502]
HTTPERROR_ALLOW_ALL = True
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 ' \
             'Safari/537.36'
DEFAULT_REQUEST_HEADERS = {
    'Referer': 'https://www.google.com/'
}

# #Crawlera
# DOWNLOADER_MIDDLEWARES = {'scrapy_crawlera.CrawleraMiddleware': 600}
# CRAWLERA_ENABLED = False
# CRAWLERA_APIKEY = '93bf0f9dfbfc4db68193445dc1e8cc99'
#
# # Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 170
CONCURRENT_REQUESTS_PER_DOMAIN = 170
# AUTOTHROTTLE_ENABLED = False
# DOWNLOAD_TIMEOUT = 600
DNSCACHE_ENABLED = True

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
# DOWNLOAD_DELAY=3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN=16
#CONCURRENT_REQUESTS_PER_IP=16

# Disable cookies (enabled by default)
COOKIES_ENABLED=False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#     ':host': 'www.google.com',
#     ':method': 'GET',
#     'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
#     'accept-language': 'ru,en-US;q=0.8,en;q=0.6,uk;q=0.4',
# }

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   # 'yellowpage.pipelines.SomePipeline': 300,
   'yellowpage.pipelines.MongoPipeline': 300,
}

MONGO_URI = '127.0.0.1'
# MONGO_URI = '198.199.69.236'
MONGO_DATABASE = 'yellowpages_ca_results'

# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
# NOTE: AutoThrottle will honour the standard settings for concurrency and delay
# AUTOTHROTTLE_ENABLED=True
# The initial download delay
# AUTOTHROTTLE_START_DELAY=0.5
# The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY=60
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG=False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED=True
#HTTPCACHE_EXPIRATION_SECS=0
# HTTPCACHE_DIR='httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES=[]
#HTTPCACHE_STORAGE='scrapy.extensions.httpcache.FilesystemCacheStorage'

CLOSESPIDER_ERRORCOUNT = 1

DOWNLOAD_HANDLERS = {'s3': None, }

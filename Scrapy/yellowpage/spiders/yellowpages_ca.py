# -*- coding: utf-8 -*-
import scrapy
import datetime
import re
import logging
import random
from pymongo import MongoClient

from user_agent import generate_user_agent
from ..items import YPCanadaItem
from scrapy.spidermiddlewares.httperror import HttpError
from ..settings import MONGO_URI
from extruct.w3cmicrodata import MicrodataExtractor


class IbilikSpider(scrapy.Spider):
    name = "yellowpages_ca"
    allowed_domains = ["yellowpages.ca"]
    search_url = "http://www.yellowpages.ca/search/si/1/{0}/{1}+{2}"

    client = MongoClient(MONGO_URI)
    db = client.yellowpages_ca

    cursor = db.yellowpages_ca.find()

    def __init__(self, *args, **kwargs):
        super(IbilikSpider, self).__init__(*args, **kwargs)
        # self.data = [row for row in self.cursor]
        self.data = [row for row in self.cursor.sort("_id", 1)][45000:]

    def start_requests(self):
        logging.info('start req')
        while self.data:
            term = self.data.pop()
            if 'count_try' not in term:
                term['count_try'] = 1

            if term['count_try'] > 1:
                continue
            kind = term['kind'].replace('\\\\', '').replace('\\', '').replace('/', '')
            url = self.search_url.format(kind.strip(), term['city'], term['province'])
            meta = {'term': term}
            yield scrapy.Request(url,  headers={'User-Agent:': generate_user_agent()},
                                 callback=self.parse, meta=meta)

    def parse(self, response):
        term = response.meta['term']

        if term['count_try'] == 1 and (response.status == 503 or response.status == 502):
            term['count_try'] += 1
            self.data.append(term)
            return None

        item = YPCanadaItem()
        if response.status == 200:
            error = response.xpath('//div[@class="mainContent"]/div[@class="errorSerp"]')
            if not error:
                count = response.xpath(
                    '//h1[@class="contentControls-head_total"]//strong[@class="totalRes"]/text()').extract()
                if not count:
                    count = response.xpath(
                        '//h1[@class="contentControls-head_total"]//strong[1]/text()').extract()
                count_surrounding = response.xpath(
                    '//div[@class="contentControls"]/h2//strong[@class="totalRes"]/text()').extract()
                count_surrounding = count_surrounding[0].strip() if count_surrounding else 0
                count = count[0].strip() if count else 0
                associated_keywords_count = 0
            else:
                count = 0
                count_surrounding = 0
                associated_keywords_count = response.xpath(
                    '//h1[@class="contentControls-head_total"]//strong[@class="totalRes"]/text()').extract()
                associated_keywords_count = associated_keywords_count[0].strip() if associated_keywords_count else 0
        else:
            count = 0
            count_surrounding = 0
            associated_keywords_count = 0

        item['count'] = count
        item['count_surrounding'] = count_surrounding
        item['associated_keywords_count'] = associated_keywords_count
        item['number_id'] = term['number_id']
        item['city_id'] = term['city_id']
        item['city'] = term['city']
        item['province'] = term['province']
        item['business_id'] = term['business_id']
        item['kind'] = term['kind']
        if count:
            item['company_info'] = self.get_companies_inf(response)
        else:
            item['company_info'] = []

        item['response_status'] = response.status
        item['url'] = response.url

        return item

    @staticmethod
    def get_companies_inf(response):
        business = response.xpath('//div[@itemprop="itemListElement"]')
        extractor = MicrodataExtractor()
        company_info = []
        for i, row in enumerate(business[:5]):
            html_content = row.extract()
            items = extractor.extract(html_content)
            if 'items' in items:
                if items['items']:
                    properties = items['items'][0]['properties']
                    name = properties.get('name')
                    url = properties.get('url')
                    if url:
                        url = url.replace('http://www.example.com/gourl?', '')
                    address = properties.get('address')

                    if address:
                        address_properties = address['properties']
                        streetAddress = address_properties.get('streetAddress')
                        addressLocality = address_properties.get('addressLocality')
                        postalCode = address_properties.get('postalCode')
                        addressRegion = address_properties.get('addressRegion')

                        if not streetAddress:
                            streetAddress = ""
                        if not addressLocality:
                            addressLocality = ""
                        if not postalCode:
                            postalCode = ""
                        if not addressRegion:
                            addressRegion = ""
                        address = ", ".join([streetAddress, addressLocality, postalCode, addressRegion]).strip()
                    else:
                        address = ""
                    phone = re.search(r"data-phone=\"([\d-]{,20})\"", html_content)
                    if phone:
                        phone = phone.group(1)
                    else:
                        phone = ""
                    if address.startswith(','):
                        address = re.sub(r"^,", "", address)
                    if address.endswith(','):
                        address = re.sub(r"$,", "", address)
                    company_info.append({"name_" + str(i + 1): name, "address_" + str(i + 1): address,
                                         "phone_" + str(i + 1): phone, 'website': url})
        return company_info

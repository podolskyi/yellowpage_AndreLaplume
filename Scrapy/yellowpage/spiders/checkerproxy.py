import scrapy


class CheckerSpider(scrapy.Spider):
    name = "checker"
    start_urls = (
        'http://www.httpbin.org/ip'
    )

    def __init__(self, *args, **kwargs):
        super(CheckerSpider, self).__init__(*args, **kwargs)
        proxy_list = self.load_proxy()
        self.proxy = proxy_list

    def start_requests(self):
        for proxy in self.proxy[:15]:
            req = scrapy.Request(self.start_urls, self.check_proxy, dont_filter=True, meta={'proxy': proxy})
            # req.meta['proxy'] = proxy
            req.meta['item'] = proxy
            yield req

    def check_proxy(self, response):
        print(response.request.meta['proxy'])
        print(response.meta['item'])
        print(response.body)

    @staticmethod
    def load_proxy():
        proxy = []
        with open('/home/yellowpage/spiders/proxy.txt') as f:
            for line in f:
                proxy.append("http://" + line.strip())
        return proxy

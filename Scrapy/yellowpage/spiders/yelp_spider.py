# -*- coding: utf-8 -*-
import scrapy
import datetime
import re
import logging
import random
import urllib


from pymongo import MongoClient
from random import choice
from bs4 import BeautifulSoup
from user_agent import generate_user_agent
from yellowpage.items import YelpItem
from scrapy.spidermiddlewares.httperror import HttpError
from yellowpage.settings import MONGO_URI


class IbilikSpider(scrapy.Spider):
    name = "yelp"
    allowed_domains = ["yelp.com"]
    search_url = "http://www.yelp.com/search?find_desc={0}&find_loc={1}%2C+{2}"

    client = MongoClient(MONGO_URI)
    db = client.yelp_dataset_part2
    db2 = client.yelp_result

    cursor = db.yelp_dataset_part2.find()
    cursor2 = db2.scrapy_items.find()

    def __init__(self, *args, **kwargs):
        super(IbilikSpider, self).__init__(*args, **kwargs)
        exists = [row['number_id'] for row in self.cursor2]
        logging.info("Exists count {0}".format(len(exists)))
        self.data = [row for row in self.cursor.sort("_id", 1) if row['number_id'] not in exists]
        # self.data = [row for row in self.cursor.sort("_id", 1)][200000:]
        logging.info("Need scraping count {0}".format(len(self.data)))
        proxy_list = self.load_proxy()
        self.proxy = proxy_list

    def start_requests(self):
        logging.info('start req')
        while self.data:
            term = self.data.pop()
            if 'count_try' not in term:
                term['count_try'] = 1

            if term['count_try'] > 1 or term['BusinessID'] == 'business_id':
                continue
            url = self.search_url.format(term['Kind'].strip(), term['Name'], term['State'])
            meta = {'term': term, 'proxy': choice(self.proxy)}
            yield scrapy.Request(url,  headers={'User-Agent:': generate_user_agent()},
                                 callback=self.parse, meta=meta, errback=lambda x:self.error_handler(x, meta))

    def parse(self, response):
        term = response.meta['term']

        if term['count_try'] == 1 and (response.status == 503 or response.status == 502):
            term['count_try'] += 1
            self.data.append(term)
            return None

        item = YelpItem()
        raw_count = response.xpath('//span[@class="pagination-results-window"]/text()').extract()
        raw_count = " ".join(raw_count) if raw_count else ""
        count = re.search(r"of (\d+)", str(raw_count))
        if count:
            count = int(count.group(1).strip())
        # if not count:
        #    count = self.parse_page(response)
        else:
            count = 0
        item['number_id'] = term['number_id']
        item['city_id'] = term['CityID']
        item['name'] = term['Name'].strip()
        item['state'] = term['State'].strip()
        item['business_id'] = term['BusinessID']
        item['kind'] = term['Kind'].strip()
        item['count'] = count
        item['response_status'] = response.status

        company_info = self.get_companies_inf(response)
        item['company_info'] = company_info
        item['url'] = response.url

        return item

    def get_companies_inf(self, response):
        html = response.body
        soup = BeautifulSoup(html, "lxml")
        rows = soup.find_all('li', class_='regular-search-result')
        company_info = []
        for i, row in enumerate(rows[:5]):
            name = row.find('a', class_="biz-name")
            if name:
                name = name.text.strip()
            else:
                name = ""
            address = row.find('address')
            if address:
                address = address.text.strip()
            else:
                address = ""
            phone = row.find('span', class_="biz-phone")
            if phone:
                phone = phone.text.strip()
            else:
                phone = ""
            company_info.append({"name_" + str(i + 1): name, "address_" + str(i + 1): address,
                                "phone_" + str(i + 1): phone})
        return company_info

    def error_handler(self, failure, meta):
        if isinstance(failure.value, HttpError):
            term = meta['term']
            term['count_try'] += 1
            self.data.append(term)

    @staticmethod
    def load_proxy():
        proxy = []
        # with open('/home/sashok/envs/yellowpages/yellowpage/spiders/proxy2.txt') as f:
        #     for line in f:
        #         proxy.append("https://" + line.strip())
        return proxy

# -*- coding: utf-8 -*-
import scrapy
import datetime
import re
import logging
import random
from pymongo import MongoClient

from user_agent import generate_user_agent
from yellowpage.items import YellowpageItem
from scrapy.spidermiddlewares.httperror import HttpError
from yellowpage.settings import MONGO_URI


class IbilikSpider(scrapy.Spider):
    name = "yellow"
    allowed_domains = ["yellowpages.com"]
    search_url = "http://www.yellowpages.com/search?search_terms={0}&geo_location_terms={1}%2C+{2}"

    client = MongoClient(MONGO_URI)
    db = client.yellowpage_data

    cursor = db.not_resolved.find()
    # cursor2 = db.yellowpage_data_part3.find()
    # cursor = db.city_state_business_1.find({"CityID": 300})
    # cursor = db.city_state_business.find()

    def __init__(self, *args, **kwargs):
        super(IbilikSpider, self).__init__(*args, **kwargs)
        self.data = [row for row in self.cursor]
        # self.data = [row for row in self.cursor.sort("_id", 1)]

    def start_requests(self):
        logging.info('start req')
        while self.data:
            term = self.data.pop()
            if 'count_try' not in term:
                term['count_try'] = 1

            if term['count_try'] > 1 or term['BusinessID'] == 'business_id':
                continue
            url = self.search_url.format(term['Kind'].strip(), term['Name'], term['State'])
            meta = {'term': term}
            yield scrapy.Request(url,  headers={'User-Agent:': generate_user_agent()},
                                 callback=self.parse, meta=meta)

    def parse(self, response):
        term = response.meta['term']

        if term['count_try'] == 1 and response.status == 503:
            term['count_try'] += 1
            self.data.append(term)
            return True

        item = YellowpageItem()
        count = response.xpath('//div[@class="pagination"]/p/text()').extract()
        if not count:
            count = self.parse_page(response)
        else:
            try:
                count = re.search(r"of (\d+)", str(count[0])).group(1)
            except:
                count = 0
        item['number_id'] = term['number_id']
        item['city_id'] = term['CityID']
        item['name'] = term['Name']
        item['state'] = term['State']
        item['business_id'] = term['BusinessID']
        item['kind'] = term['Kind']
        item['count'] = int(count) if int(term['Count']) < int(count) else term['Count']
        item['response_status'] = response.status

        company_info = self.get_companies_inf(response)
        item['company_info'] = company_info
        item['url'] = response.url

        return item

    def error_handler(self, failure, meta):
        if isinstance(failure.value, HttpError):
            term = meta['term']
            term['count_try'] += 1
            self.data.append(term)

    @staticmethod
    def parse_page(response):
        div_block = response.xpath('//div[@class="search-results organic"]/div[@class="result"]').extract()
        if div_block:
            return len(div_block)
        else:
            return "0"

    def get_companies_inf(self, response):
        div_block = response.xpath(
            '//div[@class="search-results organic"]/div[@class="result"]//div[@class="info"]')
        company_info = []
        if div_block and len(div_block) >= 5:
            for i, row in enumerate(div_block[:5]):
                name = row.xpath('h3[@class="n"]/a[@itemprop="name"]/text()').extract()
                name = name[0].strip() if name else ""
                address = row.xpath('div[@class="info-section info-primary"]/p[@itemprop="address"]//text()').extract()
                if not address:
                    address = row.xpath('div[@class="info-section info-primary"]/p[@class="adr"]/text()').extract()
                phone = row.xpath('div[@class="info-section info-primary"]/div[@itemprop="telephone"]/text()').extract()
                address = " ".join([item.strip() for item in address if item.strip()]) if address else ""
                phone = " ".join([item.strip() for item in phone if item.strip()]) if phone else ""

                company_info.append({"name_" + str(i + 1): name, "address_" + str(i + 1): address,
                                     "phone_" + str(i + 1): phone})
        elif div_block and len(div_block) < 5:
            for i, row in enumerate(div_block):
                name = row.xpath('h3[@class="n"]/a[@itemprop="name"]/text()').extract()
                name = name[0].strip() if name else ""
                address = row.xpath('div[@class="info-section info-primary"]/p[@itemprop="address"]//text()').extract()
                if not address:
                    address = row.xpath('div[@class="info-section info-primary"]/p[@class="adr"]/text()').extract()
                address = " ".join([item.strip() for item in address if item.strip()]) if address else ""
                phone = row.xpath('div[@class="info-section info-primary"]/div[@itemprop="telephone"]/text()').extract()
                phone = " ".join([item.strip() for item in phone if item.strip()]) if phone else ""

                company_info.append({"name_" + str(i + 1): name, "address_" + str(i + 1): address,
                                     "phone_" + str(i + 1): phone})

        return company_info

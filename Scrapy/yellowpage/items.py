# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class YellowpageItem(scrapy.Item):
    # define the fields for your item here like:
    number_id = scrapy.Field()
    city_id = scrapy.Field()
    name = scrapy.Field()
    state = scrapy.Field()
    business_id = scrapy.Field()
    kind = scrapy.Field()
    count = scrapy.Field()
    response_status = scrapy.Field()
    company_info = scrapy.Field()
    url = scrapy.Field()


class YelpItem(scrapy.Item):
    # define the fields for your item here like:
    number_id = scrapy.Field()
    city_id = scrapy.Field()
    name = scrapy.Field()
    state = scrapy.Field()
    business_id = scrapy.Field()
    kind = scrapy.Field()
    count = scrapy.Field()
    response_status = scrapy.Field()
    company_info = scrapy.Field()
    url = scrapy.Field()


class YPCanadaItem(scrapy.Item):
    # define the fields for your item here like:
    count = scrapy.Field()
    count_surrounding = scrapy.Field()
    associated_keywords_count = scrapy.Field()
    number_id = scrapy.Field()
    city_id = scrapy.Field()
    city = scrapy.Field()
    province = scrapy.Field()
    business_id = scrapy.Field()
    kind = scrapy.Field()
    company_info = scrapy.Field()

    response_status = scrapy.Field()
    url = scrapy.Field()

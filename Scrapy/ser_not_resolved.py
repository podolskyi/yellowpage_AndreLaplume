# -*- coding: utf-8 -*-
from __future__ import print_function
from pymongo import MongoClient
import logging


def get_data_from_db_full():
    client = MongoClient()
    db = client.yellowpages_ca

    cursor = db.yellowpages_ca.find()

    return cursor


def get_data_from_db_result():
    client = MongoClient()
    db1 = client.yellowpages_ca_results2

    cursor = db1.scrapy_items.find()

    return cursor


def insert_db(collection_name, json_data):
    client = MongoClient()
    db = client.yellowpages_ca
    collection = db[collection_name]
    collection.insert(json_data)
    client.close()


def main():
    full_data = [row['number_id'] for row in get_data_from_db_full()]
    result_data = [row['number_id'] for row in get_data_from_db_result()]

    print("Start diff")
    diff_number_id = list(set(full_data) - set(result_data))

    print("Diff len", len(diff_number_id))

    client = MongoClient()
    db3 = client.yellowpages_ca

    for i, num_id in enumerate(diff_number_id):
        cursor = db3.yellowpages_ca.find_one({'number_id': num_id}, {'_id': 0})
        print(i, cursor)
        insert_db('not_resolved', cursor)

    # insert = 0
    # for i, row in enumerate(full_data):
    #     logging.info(i)
    #     if row['number_id'] not in result_data:
    #         logging.info("========INSERT")
    #         del row['_id']
    #         insert_db('scrapy_items', row)
    #         insert += 1
    #
    # print("INSERT:", insert)


if __name__ == '__main__':
    main()
